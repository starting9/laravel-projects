<x-backend.layouts.master>

    <x-slot:title>
        PRODUCTS
    </x-slot:title>
                        <h1 class="mt-4">Products</h1>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Product Table 
                                <a class="btn btn-info btn-sm" href="{{route('products.create')}}">Add Product</a>
                                <a class="btn btn-info btn-sm" href="{{route('products.pdf')}}">Product PDF</a>
                                <a class="btn btn-info btn-sm" href="{{route('products.trash')}}">Recycle Bin</a>
                            </div>
                            <div class="card-body">
                                @if (Session::has('message'))
                                    <p class="alert alert-success">{{ Session::get('message') }}</p>
                                @endif
                                <table id="datatablesSimple">
                                    <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>Name</th>
                                            <th>Price</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($products as $product)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $product->title }}</td>
                                            <td>{{ $product->price }}</td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="{{ route('products.show', ['id' => $product->id]) }}">Show</a>
                                                <a class="btn btn-warning btn-sm" href="{{ route('products.edit', ['id' => $product->id]) }}">Edit</a>
                                                <form action="{{ route('products.destroy', ['id' => $product->id]) }}" method="POST" style="display:inline">    
                                                @csrf 
                                                @method('delete')
                                                <button type="submit " class="btn btn-danger btn-sm" onclick="return confirm('Do you want to delete this product?')">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
</x-backend.layouts.master>