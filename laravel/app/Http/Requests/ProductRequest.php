<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'exclude' => 'required|boolean',
            'title' => 'exclude_unless:exclude,false|required|min:5|max:255|unique:products,title',
            'price' => 'exclude_unless:exclude,false|required|numeric',
            'image' => 'exclude_unless:exclude,false|required|mimes:png,jpg'
        ];
    }

    public function message()
    {
        // Write Custom Error Messages
    }
}
