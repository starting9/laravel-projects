<x-backend.layouts.master>

    <x-slot:title>
        PRODUCT
    </x-slot:title>
                        <div class="card mb-4">
                            <div class="card-header">
                                <i class="fas fa-table me-1"></i>
                                Product Details 
                                <a class="btn btn-info btn-sm" href="{{route('products.create')}}">Add Product</a>
                            </div>
                            <div class="card-body">
                                <p>Title : {{ $product->title }}</p>
                                <p>Description : {{ $product->description }}</p>
                                <p>Price : {{ $product->price }}</p>
                                <img src="{{ asset('storage/products/'. $product->image) }}" />
                                <p>Created at : {{ $product->created_at->diffForHumans() }}</p>
                                <p>Updated at : {{ $product->updated_at->diffForHumans() }}</p>
                            </div>
                        </div>
</x-backend.layouts.master>