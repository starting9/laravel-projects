<?php

use App\Http\Controllers\FrontendController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/db', function () {
    return view('backend.db');
})->name('db');

Route::prefix('admin')->group(function(){

    Route::get('/products/trash', [ProductController::class, 'trash'])->name('products.trash');
    Route::get('/products/trash/{id}/restore', [ProductController::class, 'restore'])->name('products.restore');
    Route::delete('/products/trash/{id}', [ProductController::class, 'delete'])->name('products.delete');

    Route::get('/products/pdf', [ProductController::class, 'pdf'])->name('products.pdf');



    Route::get('/products', [ProductController::class, 'index'])->name('products.index');
    Route::get('/products/create', [ProductController::class, 'create'])->name('products.create');
    Route::post('/products/store', [ProductController::class, 'store'])->name('products.store');
    Route::get('/products/{id}', [ProductController::class, 'show'])->name('products.show');
    Route::get('/products/{id}/edit', [ProductController::class, 'edit'])->name('products.edit');
    Route::patch('/products/{id}', [ProductController::class, 'update'])->name('products.update');
    Route::delete('/products/{id}', [ProductController::class, 'destroy'])->name('products.destroy');
    

});

Route::get('/', [FrontendController::class, 'welcome'])->name('welcome');