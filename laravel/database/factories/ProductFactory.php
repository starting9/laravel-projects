<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $path = 'public/storage/products';

        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->realText(100),
            'image' => $this->faker->image($path, 250, 300, 'Product', false),
            'price' => $this->faker->numberBetween(100, 500)
        ];
    }
}
