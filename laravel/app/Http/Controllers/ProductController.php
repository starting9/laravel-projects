<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Barryvdh\DomPDF\Facade\Pdf;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id', 'desc')->get();    
        return view('backend.products.index' , compact('products'));
    }

    public function create()
    {    
        return view('backend.products.create');
    }

    public function store(ProductRequest $request)
    {
        try
        {
        if($request->hasFile('image'))
        {
            $file = $request->file('image');
            $fileName = time().'.'.$file->getClientOriginalExtension();
            Image::make($request->file('image'))
                ->resize(300, 300)
                ->save(storage_path().'/app/public/products/'.$fileName);
        }

        Product::create([
            'title' => $request->title,
            'price' => $request->price,
            'image' => $fileName
        ]);
        
        //Product::findOrFail($id)->create([$request->all()]); same work as above

        return redirect()->route('products.index')->withMessage('Successfully Added Product');
        
        }
        catch(QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('backend.products.show', compact('product'));
    }

    public function edit($id)
    {
        $product = Product::findOrFail($id);
        return view('backend.products.edit', compact('product'));
    }

    public function update(ProductRequest $request, $id)
    {
        try
        {
            $product = Product::findOrFail($id);


            if($request->hasFile('image'))
            {
            $file = $request->file('image');
            $fileName = time().'.'.$file->getClientOriginalExtension();
            Image::make($request->file('image'))
                ->resize(300, 300)
                ->save(storage_path().'/app/public/products/'.$fileName);
            }
            else
            {
                $fileName = $product->image;
            }

            $product->update([
                'title' => $request->title,
                'price' => $request->price,
                'image' => $fileName
            ]);
    
            //Product::findOrFail($id)->update([$request->all()]); same work as above

            return redirect()->route('products.index')->withMessage('Successfully Updated');
        
        }
        catch(QueryException $e)
        {
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
        
    }

    public function destroy($id)
    {
        Product::findOrFail($id)->delete();
        return redirect()->route('products.index')->withMessage('Successfully Deleted');
    }

    public function trash()
    {
        $products = Product::onlyTrashed()->get();
        return view('backend.products.trash', compact('products'));
    }

    public function restore($id)
    {
        Product::withTrashed()->where('id', $id)->restore();
        return redirect()->route('products.index')->withMessage('Successfully Restored'); 
    }

    public function delete($id)
    {
        Product::withTrashed()->where('id', $id)->forceDelete();
        return redirect()->route('products.index')->withMessage('Successfully Deleted'); 
    }

    public function pdf($id)
    {
        $pdf = PDF::loadView('pdf.invoice', $id);
        return $pdf->download('invoice.pdf');
    }

}
